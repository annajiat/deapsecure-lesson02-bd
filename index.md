---
layout: lesson
root: .  # Is the only page that doesn't follow the pattern /:path/index.html
permalink: index.html  # Is the only page that doesn't follow the pattern /:path/index.html
---


WEBSITE:
<https://deapsecure.gitlab.io/deapsecure-lesson02-bd/index.html>

<!-- this is an html comment -->

{% comment %} This is a comment in Liquid {% endcomment %}

> ## Prerequisites
>
> This lesson module was built for Jupyter session.
> 
{: .prereq}

{% include links.md %}
